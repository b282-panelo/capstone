import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';


export default function Banner({data}) {

    const {title, content, destination, label} = data;

return (
    <Row>
        <Col className="p-5">
            <h1 className="luna">{title}</h1>
            <p className="text">{content}</p>
            {/*<Link to={destination}>{label}</Link>*/}
            <Button variant="danger" as={Link} to={destination} >{label}</Button>

        </Col>
    </Row>
    )
}
