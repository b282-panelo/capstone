import React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';

const ProductCard = ({ name, description, price, productId }) => {
  return (
    <Card className="mt-3 mb-3 cardHighlight p-0 floral-background" style={{ borderRadius: '15px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}>
      <Card.Body>
        <Card.Title>
          <h4 className="floral-pink">{name}</h4>
        </Card.Title>
        <Card.Subtitle style={{ fontFamily: 'Arial', fontSize: '1rem', fontWeight: 'bold', color: '#888' }}>Description</Card.Subtitle>
        <Card.Text style={{ fontFamily: 'Arial', fontSize: '1rem', color: '#444' }}>{description}</Card.Text>
        <Card.Subtitle style={{ fontFamily: 'Arial', fontSize: '1rem', fontWeight: 'bold', color: '#888' }}>Price</Card.Subtitle>
        <Card.Text style={{ fontFamily: 'Arial', fontSize: '1rem', color: '#444' }}>₱ {price}</Card.Text>

        <Link to={`/products/${productId}`} className="btn btn-primary floral-green" style={{ fontFamily: 'Arial', fontSize: '1rem' }}>
          Details
        </Link>
      </Card.Body>
    </Card>
  );
};

export default ProductCard;














// import React from 'react';
// import { Card } from 'react-bootstrap';
// import { Link } from 'react-router-dom';

// export default function ProductCard({ product }) {
//   const { name, description, price } = product;

//   return (
//     <Card className="mt-3 mb-3 cardHighlight p-0">
//       <Card.Body>
//         <Card.Title>
//           <h4>{name}</h4>
//         </Card.Title>
//         <Card.Subtitle>Description</Card.Subtitle>
//         <Card.Text>{description}</Card.Text>
//         <Card.Subtitle>Price</Card.Subtitle>
//         <Card.Text>{price}</Card.Text>

//         <Link to={`/products/${product._id}`} className="btn btn-primary">
//         Details
//         </Link>
//       </Card.Body>
//     </Card>
//   );
// }


