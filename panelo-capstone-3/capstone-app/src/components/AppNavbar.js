import React, { useContext } from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import Image from 'react-bootstrap/Image';
import UserContext from '../UserContext';
import logo from '../images/logo.jpg';
import '../App.css';

function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar expand="lg" className="bg-body-tertiary fixed-top">
      <Navbar.Brand as={Link} to="/">
        <Image src={logo} className="logo" />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />

      <Navbar.Collapse id="basic-navbar-nav">
        <div className="nav-center"> {/* Center the content */}
          <Nav className="nav">
            <Nav.Link className="nav-text" as={NavLink} to="/">
              Home
            </Nav.Link>
            <Nav.Link className="nav-text" as={NavLink} to="/products">
              Products
            </Nav.Link>

            {user.isAdmin && (
              <Nav.Link className="nav-text" as={NavLink} to="/dashboard">
                Dashboard
              </Nav.Link>
            )}
          </Nav>
        </div>

        <div className="ml-auto"> {/* Align login and register links to the right */}
          {user.id ? (
            // If user is authenticated (logged in)
            <Nav className="nav">
              <Nav.Link className="nav-text" as={NavLink} to="/logout">
                Logout
              </Nav.Link>
            </Nav>
          ) : (
            // If user is not authenticated (not logged in)
            <Nav className="nav">
              <Nav.Link className="nav-text" as={NavLink} to="/login">
                Login
              </Nav.Link>
              <Nav.Link className="nav-text" as={NavLink} to="/register">
                Register
              </Nav.Link>
            </Nav>
          )}
        </div>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default AppNavbar;
