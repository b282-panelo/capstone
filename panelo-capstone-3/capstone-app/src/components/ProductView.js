import { useState, useContext, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { productId } = useParams();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const order = (productId) => {


    fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: productId
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if (data === true) {
        Swal.fire({
          title: "Successfully ordered",
          icon: "success",
          text: "You have successfully ordered the product."
        });

        // Pass the selected product to the CheckoutPage using the navigate function
        navigate("/products");

      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        });
      }
    });
  };

  const handleIncreaseQuantity = () => {
        if (quantity < 10) {
          setQuantity(prevQuantity => prevQuantity + 1);
        } else {
          // Display an alert when the user tries to order more than 10
          alert("You can only order up to 10 units.");
        }
      };
    
      const handleDecreaseQuantity = () => {
        if (quantity > 1) {
          setQuantity(prevQuantity => prevQuantity - 1);
        }
      };
    
      const calculateTotalAmount = () => {
        return price * quantity;
      };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

  return (
    <Container>
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="cardHighlight">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>

              {user.id !== null ? (
                <>
                  <div>
                    <Button variant="outline-primary" onClick={handleDecreaseQuantity}>
                      -
                    </Button>
                    <span className="mx-2">{quantity}</span>
                    <Button variant="outline-primary" onClick={handleIncreaseQuantity}>
                      +
                    </Button>
                  </div>
                  <div>Total Amount: PhP {calculateTotalAmount()}</div>

                  <Button variant="primary" onClick={() => order(productId)}>
                    Checkout
                  </Button>
                </>
              ) : (
                <div>
                  <Button className="btn btn-danger" as={Link} to="/login">
                    Log in to Checkout
                  </Button>
                  <div className="mt-2">Please log in to checkout this item.</div>
                </div>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}







