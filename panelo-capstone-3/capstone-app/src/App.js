import React, { useState, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import {UserProvider} from './UserContext';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import ProductsCatalogPage from './pages/ProductsCatalogPage';
import ProductView from './components/ProductView';
import AdminDashboardPage from './pages/AdminDashboardPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import Logout from './pages/Logout';
import Error from './pages/Error';


import './App.css';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null

  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <div className="app-container">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/products" element={<ProductsCatalogPage />} />
          <Route path="/products/:productId" element={<ProductView />} />

          {user.isAdmin === false ? (
            <Route path="/products/:productId" element={<ProductView />} />
          ) : (
            <Route path="/products/:productId" element={<Navigate to="/dashboard" replace />} />
          )}
          
          {user.isAdmin ? (
            <Route path="/dashboard" element={<AdminDashboardPage />} />
          ) : (
            <Route path="/dashboard" element={<Navigate to="/" replace />} />
          )}
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/*" element={<Error />} />
        </Routes>
        </div>
      </Router>
    </UserProvider>
  );
}

export default App;
















// import {useState, useEffect} from 'react'

// import AppNavbar from './components/AppNavbar';
// import ProductView from './components/ProductView';

// import Home from './pages/Home';
// import Products from './pages/Products';
// import Register from './pages/Register';
// import Login from './pages/Login';
// import Logout from './pages/Logout';
// import Error from './pages/Error';


// import './App.css';

// import {Container} from 'react-bootstrap';

// import {UserProvider} from './UserContext';

// import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

// function App() {

//   // const [user, setUser] = useState({email: localStorage.getItem('email')});
//   const [user, setUser] = useState({
//     id: null,
//     isAdmin: null
//   });


//   const unsetUser = () => {
//     localStorage.clear();
//   }

//   // Used to check if the user information is properly stored upon login in the localStorage and cleared upon logout
//   useEffect(() => {
//     fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
//       headers: {
//         Authorization: `Bearer ${localStorage.getItem('token')}`
//       }
//     })
//     .then(res => res.json())
//     .then(data => {

//       // user is logged in
//       if(typeof data._id !== "undefined") {
//         setUser({
//           id: data._id,
//           isAdmin: data.isAdmin
//         })
//         // user is logged out
//       } else {
//         setUser({
//           id: null,
//           isAdmin: null
//         })
//       }
//     })
//   }, []);

//   return (
//     <>
//       <UserProvider value = {{user, setUser, unsetUser}}>
//         <Router>
//           <AppNavbar />
//           <Container>
//             <Routes>
//               <Route path="/" element={<Home />} />
//               <Route path="/products" element={<Products />} />
//               <Route path="/products/:productId" element={<ProductView />} />
//               <Route path="/register" element={<Register />} />
//               <Route path="/login" element={<Login />} />
//               <Route path="/logout" element={<Logout />} />
//               <Route path="/*" element={<Error />} />
//             </Routes>
//           </Container>
//         </Router>
//       </UserProvider>    
//     </>
//   );
// }

// export default App;
