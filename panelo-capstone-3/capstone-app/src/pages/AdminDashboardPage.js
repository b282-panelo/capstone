import React, { useState, useEffect } from 'react';
import { Table, Form, Button } from 'react-bootstrap';
import '../App.css';


const AdminDashboardPage = () => {
  // States for managing products
  const [products, setProducts] = useState([]);
  const [newProduct, setNewProduct] = useState({ name: '', description: '', price: 0, isActive: true });
  const [editProduct, setEditProduct] = useState(null);

  // Function to fetch the list of all products
  const fetchProducts = () => {
    // Fetch the list of all products from the API
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}` 
      }
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.error('Error fetching products:', error);
      });
  };

  useEffect(() => {
    fetchProducts();
  }, []);


  // Function to handle form submission for adding or updating a product
  const handleSubmit = (e) => {
    e.preventDefault();

    const updatedProduct = { ...newProduct };

    if (editProduct) {
      // Update existing product
      fetch(`${process.env.REACT_APP_API_URL}/products/${editProduct._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(updatedProduct)
      })
      .then((res) => res.json())
      .then((data) => {
        if(data.value === true) {
          console.log('Update added to product:', updatedProduct);
          fetchProducts();
        } else {
          console.log('Failed to udpate product:', data.error);
        }
      })
      .catch((error) => {
        console.error('Error updating product:', error);
      })

    } else {
      // Add new product
      fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(newProduct)
      })
      .then((res) => res.json())
      .then((data) => {
        if(data.value === true) {
          console.log('New product added:', newProduct);
          fetchProducts();
        } else {
          console.error('Failed to add product:', data.error);
        }
      })
      .catch((error) => {
        console.error('Error adding product:', error);
      })
      
      console.log('New product:', newProduct);
    }


    setNewProduct({ name: '', description: '', price: 0, isActive: true });
    setEditProduct(null);
    fetchProducts();
  };

  // Function to handle product deactivation/reactivation
  const handleToggleProduct = (productId, isActive) => {
  
  // Deactivate or reactivate the product based on its current status
  const endpoint = isActive
    ? `${process.env.REACT_APP_API_URL}/products/${productId}/archive`
    : `${process.env.REACT_APP_API_URL}/products/${productId}/activate`;

    fetch(endpoint, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
    })
    .then((res) => res.json())
    .then((data) => {
      
      console.log(data);

      fetchProducts();
    })
    .catch((error) => {
      console.error('Error toggling product:', error);
    });
  };

  // Function to handle product edit
  const handleEditProduct = (product) => {
    setEditProduct(product);
    setNewProduct({ ...product });
  };

  // Function to handle product delete
  const handleDeleteProduct = (productId) => {

    fetch(`${process.env.REACT_APP_API_URL}/products/delete/${productId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}` 
      }
    })
    .then((res) => res.json())
    .then((data) => {
      
      console.log('Delete product:', productId);

      fetchProducts();
    })
    .catch((error) => {
      console.error('Error deleting product:', error);
    });

  };

  return (
   
      <div className="container">
      <h2 className="girly-title">Product Management</h2>

      {/* Table to display list of products */}
      <Table striped bordered hover className="girly-table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(products) && products.map((product) => (
            <tr key={product._id}>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>₱ {product.price}</td>
              <td>{product.isActive ? 'Active' : 'Inactive'}</td>
              <td>
                <Button variant="primary" onClick={() => handleEditProduct(product)}>
                  Edit
                </Button>{' '}
                <Button
                  variant={product.isActive ? 'danger' : 'success'}
                  onClick={() => handleToggleProduct(product._id, product.isActive)}
                >
                  {product.isActive ? 'Deactivate' : 'Reactivate'}
                </Button>{' '}
                <Button variant="danger" onClick={() => handleDeleteProduct(product._id)}>
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>



      {/* Form to add new product or update existing product */}
      <Form className="girly-form" onSubmit={handleSubmit}>
        <Form.Group controlId="productName">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter product name"
            value={newProduct.name}
            onChange={(e) => setNewProduct({ ...newProduct, name: e.target.value })}
            required
          />
        </Form.Group>

        <Form.Group controlId="productDescription">
          <Form.Label>Description</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            placeholder="Enter product description"
            value={newProduct.description}
            onChange={(e) => setNewProduct({ ...newProduct, description: e.target.value })}
            required
          />
        </Form.Group>

        <Form.Group controlId="productPrice">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            step=" "
            placeholder="Enter product price"
            value={newProduct.price}
            onChange={(e) => setNewProduct({ ...newProduct, price: e.target.value })}
            required
          />
        </Form.Group>

        <Form.Group controlId="productStatus">
          <Form.Check
            type="checkbox"
            label="Active"
            checked={newProduct.isActive}
            onChange={(e) => setNewProduct({ ...newProduct, isActive: e.target.checked })}
          />
        </Form.Group>

        <Button className="submit-button" variant="primary" type="submit">
          {editProduct ? 'Update Product' : 'Add Product'}
        </Button>
      </Form>
    </div>
   
    
  );
};

export default AdminDashboardPage;



