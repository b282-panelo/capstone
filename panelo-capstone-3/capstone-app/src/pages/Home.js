import Banner from '../components/Banner';
import '../App.css'

export default function Home() {

	const data = {
		title: "Luna's Flower Boutique",
		content: "Find your own Happiness. Deliver to any corner of the city.",
		destination: "/products",
		label: "Pick a Bouquet"
	}


	return (
		<>
			<div className="home-background">
				<Banner data={data} />
			</div>
		</>
	)
}