// import React from 'react';
import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import '../App.css';

const ProductsCatalogPage = () => {

  const [products, setProducts] = useState([]);

  useEffect(() => {
    // Fetch the list of active products from the API
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.error('Error fetching products:', error);
      });
  }, []);

  return (
    <Container>
      <Row>
        {Array.isArray(products) &&
          products.map((product) => (
            <Col key={product._id} md={4}>
              <ProductCard
                name={product.name}
                description={product.description}
                price={product.price}
                productId={product._id}
              />
            </Col>
          ))}
      </Row>
    </Container>
  );
};

export default ProductsCatalogPage;

















// import React, { useState, useEffect } from 'react';
// import ProductCard from '../components/ProductCard';

// const ProductsCatalogPage = () => {
//   const [products, setProducts] = useState([]);

//   useEffect(() => {
//     // Fetch the list of active products from the API
//     fetch(`${process.env.REACT_APP_API_URL}/products/active`)
//       .then((res) => res.json())
//       .then((data) => {
//         setProducts(data);
//       })
//       .catch((error) => {
//         console.error('Error fetching products:', error);
//       });
//   }, []);

//   return (
//     <div>
//       <h2>Products Catalog</h2>
//       {products.length === 0 ? (
//         <p>No active products found.</p>
//       ) : (
//         products.map((product) => (
//           <ProductCard key={product._id} product={product} />
//         ))
//       )}
//     </div>
//   );
// };

// export default ProductsCatalogPage;