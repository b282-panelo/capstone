const express = require("express");

const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");


// Route for product creation
router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all the products

router.get("/all", auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    };

    productController.getAllProducts(data).then(resultFromController => res.send(resultFromController))
});




// Route for retrieving active products
router.get("/active", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for retrieve specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params, req.body, data).then(resultFromController => res.send(resultFromController));
});



// Route for archiving a product
router.patch("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});



// Route for activating a product
router.patch("/:productId/activate", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.activateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});



// // Route for retrieving all orders (Admin only)

// router.get("/allOrders", auth.verify, (req, res) => {

// 	const data = {
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 		productId: auth.decode(req.headers.authorization).id
// 	}

// 	userController.allOrders(data).then(resultFromController => res.send(resultFromController));
// });


// Route for deleting product (Admin only)

router.delete("/delete/:productId", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: auth.decode(req.headers.authorization).id
	}

	productController.deleteProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});













module.exports = router;