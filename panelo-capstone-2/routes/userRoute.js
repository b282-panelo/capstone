const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");


// // Route for checking if user's email already exist
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for Non-Admin registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for Admin registration
router.post("/register/admin", (req, res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication (login)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});


// Route for Non-admin User checkout (Create Order)

router.post("/order",auth.verify, (req,res) =>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId  : auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	userController.order(data).then(resultFromController => res.send(resultFromController));
}) 


// Route for setting user as admin

router.patch("/:userId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.setAsAdmin(req.params, data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving authenticated user's orders

router.get("/myOrders/:userId", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}


	userController.usersOrder(data, req.params).then(resultFromController => res.send(resultFromController));
});





















module.exports = router;