// require mongoose
const mongoose = require("mongoose");

// productSchema
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required!"]
	},
	description: {
		type: String,
		required: [true, "Description is required!"]
	},
	price: {
		type: Number,
		required: [true, "Price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [
		{
			userId: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'User',
				required: [true, "UserId is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			orderId: String
		}
	]
});

module.exports = mongoose.model("Product", productSchema);