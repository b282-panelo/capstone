// require mongoose
const mongoose = require("mongoose");

// userSchema
const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	orderedProduct : [
		{
			products : [
				{
					productId : {
						type : mongoose.Schema.Types.ObjectId,
						ref: 'Product',
						required : [true, "Product ID is required"]
					},
					productName : String,
					quantity : {
						type: Number,
						default: 1
					}
					
				}
			],
			totalAmount : Number
		}
	]
});

module.exports = mongoose.model("User", userSchema);









