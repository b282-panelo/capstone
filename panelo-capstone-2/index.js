const express = require("express");

const mongoose = require("mongoose");

const cors = require("cors");

const userRoute = require("./routes/userRoute");

const productRoute = require("./routes/productRoute");

const app = express();


// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://nicoppanelo:txVgMMbI4XrzsZpU@cluster0.ftsxh0d.mongodb.net/eCommerceAPICapstone",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true	
	}
);


// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all resources to access backend
app.use(cors());

app.use("/users", userRoute);

app.use("/products", productRoute);


app.listen(process.env.PORT || 4001, () => console.log(`Now listening to port ${process.env.PORT || 4001}!`));