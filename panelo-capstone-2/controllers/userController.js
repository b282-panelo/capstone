const User = require("../models/User");

const Product = require("../models/Product");

const bcrypt = require("bcrypt");

const auth = require("../auth");


// Controller for checking if email exists

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		} else {
			return false;
		}
	})
}


// Controllers for Non-Admin registration

module.exports.registerUser = (reqBody) => {
	

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
};


// Controllers for Admin registration

module.exports.registerAdmin = (reqBody) => {
	

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		isAdmin: true,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
};



// Controllers for user authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(
					result)}
			} else {
				return false;
			};
		};
	});
};


// Controller for retrieving user details
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};



// Controller for user checkout (Create Order)
module.exports.order = async (data) => {
  
    if (data.isAdmin) {
      	return "User must be non-admin to access this.";
    }

    let isUserUpdated = await User.findById(data.userId);
    	if (!isUserUpdated) {
      		return "User not found";
    }
	
	let isProductUpdated = await Product.findById(data.productId);
    	if (!isProductUpdated) {
      		return "Product not found";
    }

    let quantity = isUserUpdated.orderedProduct.length + 1;
    
    let totalAmount = isProductUpdated.price * quantity;

    isUserUpdated.orderedProduct.push({ 
    	products: [
        {
          productId: data.productId,
          productName: isProductUpdated.name,
          quantity: quantity
        }
      	],
      	totalAmount: totalAmount,
      	purchasedOn: new Date()
    });

    await isUserUpdated.save();

    isProductUpdated.userOrders.push({ userId: data.userId});
    await isProductUpdated.save();

    return true;
  
};



// Controller for setting a user as an admin
module.exports.setAsAdmin = (reqParams, data) => {
	
	if(data.isAdmin) {
		let updateUser = {
		isAdmin: true
	};

		return User.findByIdAndUpdate(reqParams.userId, updateUser).then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	}
	
	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};


// Controller for retrieving authenticated user's order

module.exports.usersOrder = (data, reqParams) => {


	return User.findById(reqParams.userId).then(result => {
		if (result) {
			return result.orderedProduct
		} else {
			return 'Error occurred.'
		}
	});


	let message = Promise.resolve("User must be non-Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};


