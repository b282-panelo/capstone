const Product = require("../models/Product");




// Controller for creating new product

module.exports.addProduct = (data) => {
	
	if (data.isAdmin) {
			
		let newProduct = new Product({
				name : data.product.name,
				description : data.product.description,
				price : data.product.price
			});
				
			return newProduct.save().then((product, error) => {
					
				if (error) {
					return false;
					
				} else {
					return true;
				};
			});

		};
		
	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};
















// Controller for creating new product

// module.exports.addProduct = (data) => {
	
// 	if (data.isAdmin) {
			
// 		let newProduct = new Product({
// 				name : data.product.name,
// 				description : data.product.description,
// 				price : data.product.price
// 			});
				
// 			return newProduct.save().then((product, error) => {
					
// 				if (error) {
// 					return false;
					
// 				} else {
// 					return true;
// 				};
// 			});

// 		};
		
// 	let message = Promise.resolve("User must be Admin to access this.")
// 	return message.then((value) => {
// 		return {value}
// 	});
// };


// Controllers for retrieving all the products (Admin only)

module.exports.getAllProducts = (data) => {
  	if (data.isAdmin === true) {
    	return Product.find({}).then(result => {
        	return result;
      	})
      
  	} 

  	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};


// Controllers for retrieving active products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


// Controllers for retrieving specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Controllers for updating a product
module.exports.updateProduct = (reqParams, reqBody, data) => {

	if(data.isAdmin === true) {

		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if (error) {
				return false
			} else {
				return true;
			}
		});
	};

	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});

};



// Controllers for archiving a product

module.exports.archiveProduct = (reqParams, data) => {
	
	if(data.isAdmin){

		let updateActiveField = {
			isActive: false
		}

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((result, error) => {
			if(error) {
				return false;
			} else {
				return true
			}
			
		});
	}

	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};



// Controllers for activating a product

module.exports.activateProduct = (reqParams, data) => {
	
	if(data.isAdmin){

		let updateActiveField = {
			isActive: true
		}

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((result, error) => {
			if(error) {
				return false;
			} else {
				return true
			}
			
		});
	}

	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};


// Controller for deleting product

module.exports.deleteProduct = (reqParams, data) => {

	if(data.isAdmin) {
		return Product.findByIdAndDelete(reqParams.productId).then(result => {
			return true;
		});
	}
};


// // Controller for retrieving authenticated user's order

// module.exports.usersOrder = (data, reqParams) => {


// 	return User.findById(reqParams.userId).then(result => {
// 		if (result) {
// 			return result.orderedProduct
// 		} else {
// 			return 'Error occurred.'
// 		}
// 	});


// 	let message = Promise.resolve("User must be non-Admin to access this.")
// 	return message.then((value) => {
// 		return {value}
// 	});
// };